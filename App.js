import React, { useState } from 'react'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducer from './src/reducers/authReducer'
import { useFonts, LondrinaSolid_400Regular } from '@expo-google-fonts/londrina-solid'
import { SplashScreen } from './src/components'
import RootApp from './src/RootApp'

const store = createStore(reducer)

export default function App() {
  let [fontsLoaded] = useFonts({
    LondrinaSolid_400Regular,
  });

  if (!fontsLoaded) {
    return <SplashScreen />;
  }

  return (
    <Provider store={store}>
      <RootApp/>
    </Provider>
  );
}