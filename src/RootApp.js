import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { StatusBar } from 'expo-status-bar'
import { connect } from 'react-redux'

import { AuthTabBar, HomeScreen, AuthLoginScreen, AuthRegisterScreen, AnimeDetailScreen, BrowseSeasonScreen, AboutDevScreen } from './components'

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'

const RootStack = createStackNavigator()
const RootStackScreen = ({ userToken }) => (
  <RootStack.Navigator headerMode='none'>
    {userToken ? (
      <RootStack.Screen name="HomeStack" component={HomeStackScreen} />
    ) : (
      <RootStack.Screen name="AuthStack" component={AuthTabsScreen} />
    )}
  </RootStack.Navigator>
)

const HomeStack = createStackNavigator()
const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
    <HomeStack.Screen name="BrowseSeason"
      component={BrowseSeasonScreen}
      options={{
        title: "Browse Anime by Season",
        headerStyle: {
          backgroundColor: '#4488A9'
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontFamily: "LondrinaSolid_400Regular",
        }
      }}
    />
    <HomeStack.Screen name="AnimeDetail"
      component={AnimeDetailScreen}
      options={({ route }) => ({
        title: "Anime Details",
        headerStyle: {
          backgroundColor: '#4488A9',
          elevation: 0
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: "LondrinaSolid_400Regular",
        }
      })}
    />
    <HomeStack.Screen name="AboutDev"
      component={AboutDevScreen}
      options={{
        title: "About the Dev",
        headerStyle: {
          backgroundColor: '#4488A9',
          elevation: 0
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: "LondrinaSolid_400Regular",
        }
      }}
    />
  </HomeStack.Navigator>
)

const AuthTabs = createMaterialTopTabNavigator()
const AuthTabsScreen = () => (
  <AuthTabs.Navigator
    style={{ backgroundColor: "#77C3DC" }}
    swipeEnabled={false}
    tabBar={props => <AuthTabBar {...props} />}
  >
    <AuthTabs.Screen name="Register" component={AuthRegisterScreen} />
    <AuthTabs.Screen name="Login" component={AuthLoginScreen} />
  </AuthTabs.Navigator>
)

class RootApp extends Component {
  render() {
    const { userToken } = this.props
    return (
      <NavigationContainer>
        <RootStackScreen userToken={userToken}/>
        <StatusBar hidden={true} />
      </NavigationContainer>
    )
  }
}

function mapStateToProps(state) {
  return {
    userToken: state.userToken
  }
}

export default connect(mapStateToProps, null)(RootApp)