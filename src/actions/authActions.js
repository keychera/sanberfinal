
function hash(password) {
  return "asdf"
}

export const signIn = ({ username, password }) => {
  return {
    type: 'SIGNIN',
    payload: {
      hash: hash(password),
      username: username
    }
  }
}

export const signUp = ({ username, password }) => {
  return {
    type: 'SIGNUP',
    payload: {
      hash: hash(password),
      username: username
    }
  }
}

export const signOut = () => {
  return {
    type: 'SIGNOUT'
  }
}
