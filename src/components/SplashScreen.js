import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class SplashScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#4488A9", justifyContent: "center", alignItems: "center" }}>
        <Icon style={{color: "#77C3DC"}} name="border-none-variant" size={75} />
      </View>
    )
  }
}

