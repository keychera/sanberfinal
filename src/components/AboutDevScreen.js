import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'

export default class AboutDevScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.profile}>
          <Image style={styles.profilePic} source={require('../../assets/sienna.jpg')} />
          <Text style={styles.profileText}>Kevin Erdiza</Text>
        </TouchableOpacity>
        <View style={styles.detail}>
          <DetailCard iconName="github" detailText="github.com/keychera"/>
          <DetailCard iconName="gitlab" detailText="gitlab.com/keychera"/>
          <DetailCard iconName="twitter" detailText="@keychera"/>
        </View>
      </View>
    )
  }
}

const DetailCard = ({iconName, detailText}) => (
  <TouchableOpacity style={styles.detailCard}>
    <Icon style={styles.detailIcon} name={iconName} />
    <Text style={styles.detailText}>{detailText}</Text>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4488A9"
  },
  profile: {
    height: 80,
    borderRadius: 50,
    backgroundColor: "#77C3DC",
    marginHorizontal: 40,
    marginVertical: 20,
    flexDirection: "row",
    alignItems: "center",
    elevation: 10
  },
  profilePic: {
    width: 100,
    height: 100,
    borderRadius: 50
  },
  profileText: {
    color: "white",
    fontSize: 28,
    marginLeft: 10,
    fontFamily: "LondrinaSolid_400Regular",
  },
  detail: {
    paddingVertical: 20,
    paddingHorizontal: 30
  },
  detailCard: {
    backgroundColor: "white",
    flexDirection: "row",
    alignContent: "flex-start",
    borderRadius: 50,
    padding: 10,
    marginBottom: 10
  },
  detailIcon: {
    fontSize: 30
  },
  detailText: {
    alignSelf: "center",
    marginLeft: 10,
    fontSize: 18,
    fontFamily: "LondrinaSolid_400Regular",
  }
})