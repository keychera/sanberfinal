import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class AuthTabBar extends Component {
  // below is a modified function MyTabBar, originally
  // copied from from https://reactnavigation.org/docs/bottom-tab-navigator
  render() {
    const { state, descriptors, navigation } = this.props
    const focusedOptions = descriptors[state.routes[state.index].key].options;

    if (focusedOptions.tabBarVisible === false) {
      return null;
    }

    return (
      <View style={styles.tabArea}>
        <Icon style={styles.mainIcon} name="border-none-variant" size={75} />
        <View style={styles.tabs}>
          {state.routes.map((route, index) => {
            const { options } = descriptors[route.key];
            const label =
              options.tabBarLabel !== undefined
                ? options.tabBarLabel
                : options.title !== undefined
                  ? options.title
                  : route.name;

            const isFocused = state.index === index;

            const onPress = () => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!isFocused && !event.defaultPrevented) {
                navigation.navigate(route.name);
              }
            };

            const onLongPress = () => {
              navigation.emit({
                type: 'tabLongPress',
                target: route.key,
              });
            };

            return (
              <TouchableOpacity
                accessibilityRole="button"
                accessibilityStates={isFocused ? ['selected'] : []}
                accessibilityLabel={options.tabBarAccessibilityLabel}
                testID={options.tabBarTestID}
                onPress={onPress}
                onLongPress={onLongPress}
                style={[styles.tabButton, { backgroundColor: isFocused ? "#3B7794" : "#3B779400" }]}
                key={route.key}
              >
                <Text style={[styles.tabText, { color: isFocused ? '#FFFFFF' : '#85BCD7' }]}>
                  {label}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tabArea: {
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#4488A9"
  },
  mainIcon: {
    color: "#77C3DC",
    paddingVertical: 60
  },
  tabs: {
    flexDirection: 'row',
    paddingBottom: 15,
    paddingHorizontal: 80
  },
  tabButton: {
    flex: 1,
    padding: 5,
    borderRadius: 25
  },
  tabText: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: "LondrinaSolid_400Regular"
  }
})