import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon2 from 'react-native-vector-icons/MaterialIcons'
import Toggle from './Toggle';

export default class BrowseSeasonScreen extends Component {
  constructor(props) {
    super(props)
    const selectedYear = props.route.params.seasonData.season_year || 1997
    const selectedSeason = 'Summer'
    this.state = {
      selectedYear,
      selectedSeason,
      yearOps: [selectedYear-3, selectedYear - 2, selectedYear - 1, selectedYear],
      seasonData: props.route.params.seasonData
    }
  }

  async fetchSeason(year, season) {
    const response = await fetch(`https://api.jikan.moe/v3/season/${year}/${season}`)
    const json = await response.json()
    this.setState({ seasonData: json })
  }

  render() {
    const yearOps = this.state.yearOps
    const animeListData = (this.state.seasonData || {}).anime || []
    return (
      <View style={styles.container}>
        <View style={styles.optionArea}>
          <Toggle
            value={this.state.selectedYear}
            options={yearOps || []}
            onChange={(year) => {
              this.setState({ selectedYear: year, seasonData: null})
              const { selectedSeason } = this.state
              this.fetchSeason(year, selectedSeason)
            }}
          />
          <Toggle
            value={this.state.selectedSeason}
            options={['Winter', 'Spring', 'Summer', 'Fall']}
            onChange={(season) => {
              this.setState({ selectedSeason: season, seasonData: null})
              const { selectedYear } = this.state
              this.fetchSeason(selectedYear, season)
            }}
          />
        </View>
        <View style={styles.resultArea}>
          <FlatList
            data={animeListData}
            contentContainerStyle={styles.animeListContainer}
            showsHorizontalScrollIndicator={false}
            renderItem={(season, i) => {
              const anime = season.item
              const { title, image_url } = anime
              return (
                <TouchableOpacity
                  style={styles.resultCard}
                  onPress={() => this.props.navigation.push("AnimeDetail", { animeData: anime })}
                >
                  <Image style={styles.resultCardImage} source={{ uri: image_url }} />
                  <View style={styles.resultCardDetail}>
                    <Text style={styles.titleText}>{title}</Text>
                  </View>
                  <Icon2 style={styles.selectCard} name="navigate-next" />
                </TouchableOpacity>
              )
            }}
            keyExtractor={(item, i) => String(item.mal_id)}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  resultArea: {
    paddingHorizontal: 20,
    flex: 1
  },
  animeListContainer: {
    margin: 10
  },
  resultCard: {
    backgroundColor: "#77C3DC",
    borderRadius: 20,
    height: 120,
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  resultCardImage: {
    width: 90,
    height: 120,
    borderRadius: 20,
  },
  resultCardDetail: {
    flex: 1,
    paddingHorizontal: 15,
    justifyContent: "flex-start"
  },
  titleText: {
    fontFamily: "LondrinaSolid_400Regular",
    color: "white",
    fontSize: 16,
  },
  selectCard: {
    fontSize: 60,
    color: "white"
  }
})