import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'

// modified Toggle class from http://www.reactnativeexpress.com/flexbox
export default class Toggle extends Component {

  onPress = (option) => {
    const { onChange } = this.props

    onChange(option)
  }

  renderOption = (option, i) => {
    const { value } = this.props

    return (
      <TouchableOpacity
        style={[styles.option, option === value && styles.activeOption]}
        onPress={this.onPress.bind(this, option)}
        key={i}
      >
        <Text style={[styles.text, option === value && styles.activeText]}>
          {option}
        </Text>
      </TouchableOpacity>
    )
  }

  render() {
    const { label, options } = this.props

    return (
      <View style={styles.container}>
        <View style={styles.optionsContainer}>
          {options.map(this.renderOption)}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  text: {
    color: "#828282",
    fontFamily: "LondrinaSolid_400Regular",
    fontSize: 18
  },
  activeText: {
    color: "white"
  },
  optionsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  option: {
    padding: 4,
    backgroundColor: 'whitesmoke',
    marginRight: 5,
    borderRadius: 10
  },
  activeOption: {
    backgroundColor: '#77C3DC',
  },
})