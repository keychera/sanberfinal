import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';

export default class AnimeDetailScreen extends Component {
  render() {
    const { animeData } = (this.props.route || {}).params || {}
    const {
      image_url, mal_id, title, episodes,
      synopsis, producers, genres, score
    } = animeData
    return (
      <View style={styles.container}>

        <View style={styles.topArea}>
          <View style={styles.imagePanel}>
            <Image style={styles.animeCover} source={{ uri: image_url }} />
          </View>

          <View style={styles.detailPanel}>
            <Text style={styles.producer}>{(producers || [{ name: '?' }])[0].name}</Text>
            <View style={styles.genreContainer}>
              {genres.map((genre, i) => (
                <View style={styles.genreCard} key={i}>
                  <Text style={styles.genreText}>{genre.name}</Text>
                </View>
              ))}
            </View>
            <Text>{`${episodes || '?'} eps`}</Text>
            <Text>{score}</Text>
          </View>
        </View>

        <View style={styles.bottomArea}>
          <View style={styles.titlePanel}>
            <Text style={styles.titleText}>{title}</Text>
          </View>
          <ScrollView style={styles.synopsisPanel} showsVerticalScrollIndicator={false}>
            <Text style={styles.synopsisText}>{synopsis}</Text>
          </ScrollView>
          <View style={styles.actionPanel}>

          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4488A9"
  },
  topArea: {
    height: 192,
    flexDirection: "row",
    paddingHorizontal: 20
  },
  imagePanel: {
    width: 138,
    height: 192,
    borderRadius: 15,
    backgroundColor: "white"
  },
  animeCover: {
    width: 138,
    height: 192,
    borderRadius: 15
  },
  detailPanel: {
    flex: 1,
    height: 192,
    marginLeft: 20,
    borderRadius: 15,
    backgroundColor: "white",
    padding: 5
  },
  producer: {
    color: "#828282",
    fontSize: 12
  },
  genreContainer: {
    flexDirection: "row",
    flexWrap: "wrap"
  },
  genreCard: {
    padding: 3,
    backgroundColor: "#4488A9",
    borderRadius: 10,
    margin: 2
  },
  genreText: {
    color: "white",
    fontSize: 12
  },
  bottomArea: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 20
  },
  titlePanel: {
    borderRadius: 15,
    backgroundColor: "white",
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  titleText: {
    fontFamily: "LondrinaSolid_400Regular",
    color: "#77C3DC",
    fontSize: 24,
  },
  synopsisPanel: {
    borderRadius: 15,
    marginTop: 20,
    backgroundColor: "white",
    paddingHorizontal: 20,
  },
  synopsisText: {
    marginVertical: 20,
  },
})
