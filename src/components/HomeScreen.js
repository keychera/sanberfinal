import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Icon2 from 'react-native-vector-icons/MaterialIcons'

import { connect } from 'react-redux';
import { signOut } from '../actions/authActions'

class HomeScreen extends Component {

  state = {
    currentSeasondata: null
  }

  componentDidMount() {
    this.fetchCurrentSeason()
  }

  async fetchCurrentSeason() {
    const response = await fetch('https://api.jikan.moe/v3/season/')
    const json = await response.json()
    this.setState({ currentSeasondata: json })
  }

  styleSeasonText(season) {
    switch (season) {
      case 'Summer':
        return { color: '#F44336' }
      default:
        return { color: '#4488A9' }
    }
  }

  render() {
    const name = this.props.username || 'HUNWIZ'

    const currentSeasondata = this.state.currentSeasondata
    const season = (currentSeasondata || { season_name: 'Loading...' }).season_name
    const animeListData = (currentSeasondata || { anime: [] }).anime.slice(0, 4)
    return (
      <View style={styles.container}>
        <View style={styles.panel}>

        </View>
        <View style={styles.midPanel}>
          <TouchableOpacity style={styles.profilePictureContainer}>
            <Image style={styles.profilePicture} source={require('../../assets/hunwiz.jpg')} />
          </TouchableOpacity>
          <View style={styles.greetingContainer}>
            <Text style={styles.hello}>HELLO,</Text>
            <Text style={styles.username}>{`${name}!`}</Text>
          </View>
          <View style={styles.rightPanel}>
            <TouchableOpacity
              style={[styles.rightPanelButton, { backgroundColor: "#f44336" }]}
              onPress={this.props.signOut}
            >
              <Icon style={[styles.rightPanelIcon, { color: "white" }]} name="logout" />
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.rightPanelButton, { backgroundColor: "#77C3DC" }]}
              onPress={() => this.props.navigation.push("AboutDev")}
            >
              <Icon style={[styles.rightPanelIcon, { color: "white" }]} name="information-variant" />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.panel}>

          <TouchableOpacity
            style={styles.bottomPanelTopArea}
            onPress={currentSeasondata
              ? () => this.props.navigation.push('BrowseSeason', { seasonData: currentSeasondata })
              : () => null
            }
          >
            <View style={styles.bottomPanelHeader}>
              <Text style={styles.browseText}>Browse Anime</Text>
              <View style={styles.seasonContainer}>
                <Text style={styles.thisText}>{currentSeasondata ? 'THIS' : ''}</Text>
                <Text style={[styles.seasonText, this.styleSeasonText(season)]}>{season}</Text>
              </View>
            </View>
            <Icon2 style={styles.browseButton} name="navigate-next" />
          </TouchableOpacity>

          <FlatList
            data={animeListData}
            contentContainerStyle={styles.animeListContainer}
            showsHorizontalScrollIndicator={false}
            renderItem={(season, i) => {
              const anime = season.item
              return (
                <View>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.push("AnimeDetail", { animeData: anime })}
                  >
                    <Image style={styles.animeCover} source={{ uri: anime.image_url }} />
                  </TouchableOpacity>
                </View>
              )
            }}
            keyExtractor={(item, i) => String(item.mal_id)}
            horizontal={true}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4488A9",
    justifyContent: "center",
    alignItems: "stretch",
    paddingVertical: 20
  },
  panel: {
    flex: 1,
    backgroundColor: "white",
    borderRadius: 32,
    marginHorizontal: 30,
    marginVertical: 20,
    elevation: 10,
    padding: 15
  },
  midPanel: {
    height: 120,
    marginLeft: 30,
    flexDirection: "row",
    alignItems: "center"
  },
  profilePictureContainer: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: "#77C3DC",
    elevation: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  profilePicture: {
    width: 100,
    height: 100,
    borderRadius: 50
  },
  greetingContainer: {
    flex: 1,
    flexDirection: "column",
    marginLeft: 15
  },
  hello: {
    fontFamily: "LondrinaSolid_400Regular",
    fontSize: 16,
    color: "#DDDDDD"
  },
  username: {
    fontFamily: "LondrinaSolid_400Regular",
    fontSize: 36,
    color: "white"
  },
  rightPanel: {
    flex: 0.5,
    backgroundColor: "white",
    borderTopLeftRadius: 32,
    borderBottomLeftRadius: 32,
    elevation: 10,
    paddingVertical: 5,
  },
  rightPanelButton: {
    marginHorizontal: 15,
    marginVertical: 5,
    borderRadius: 50,
    padding: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  rightPanelIcon: {
    fontSize: 24,
  },
  bottomPanelTopArea: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center"
  },
  browseButton: {
    fontSize: 48,
    color: "#828282"
  },
  bottomPanelHeader: {
    flexDirection: "column"
  },
  browseText: {
    fontFamily: "LondrinaSolid_400Regular",
    fontSize: 20,
    color: "#77C3DC"
  },
  seasonContainer: {
    flexDirection: "row"
  },
  thisText: {
    fontFamily: "LondrinaSolid_400Regular",
    fontSize: 20,
    color: "#4488A9",
    marginRight: 8
  },
  seasonText: {
    fontFamily: "LondrinaSolid_400Regular",
    fontSize: 30,
  },
  animeListContainer: {
    marginVertical: 6
  },
  animeCover: {
    height: 90,
    width: 60,
    borderRadius: 10,
    marginHorizontal: 10
  }
})

export default connect((state) => ({
  username: state.username
}), { signOut })(HomeScreen)