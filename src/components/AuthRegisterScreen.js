import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import { signUp } from '../actions/authActions'
import { connect } from 'react-redux';

class AuthRegisterScreen extends Component {
  state = {
    username: '',
    password: '',
    confirmPassword: ''
  }

  getAuthPayload() {
    return {
      username: this.state.username,
      password: this.state.password
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.panel}>
          <TextInput
            value={this.state.username}
            onChangeText={(username) => this.setState({ username })}
            placeholder='Username'
            placeholderTextColor="white"
            style={styles.input}
          />
          <TextInput
            value={this.state.password}
            onChangeText={(password) => this.setState({ password })}
            placeholder='Password'
            placeholderTextColor="white"
            secureTextEntry={true}
            style={styles.input}
          />
          <TextInput
            value={this.state.confirmPassword}
            onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
            placeholder='Confirm Password'
            placeholderTextColor="white"
            secureTextEntry={true}
            style={styles.input}
          />
          <TouchableOpacity
            style={styles.confirmButton}
            onPress={() => this.props.signUp(this.getAuthPayload())}
          >
            <Text style={styles.confirmButtonText}>Confirm</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4488A9",
    alignItems: "center"
  },
  panel: {
    marginTop: 5,
    borderRadius: 32,
    width: 200,
    height: 280,
    backgroundColor: "white",
    paddingVertical: 25,
    paddingHorizontal: 10,
    justifyContent: "center"
  },
  input: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 12,
    marginHorizontal: 18,
    marginVertical: 12,
    backgroundColor: "#C4C4C4",
    fontFamily: "LondrinaSolid_400Regular"
  },
  confirmButton: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    marginHorizontal: 36,
    marginVertical: 12,
    backgroundColor: "#8EA66F",
    borderRadius: 12,
    alignItems: "center"
  },
  confirmButtonText: {
    fontSize: 18,
    fontFamily: "LondrinaSolid_400Regular",
    color: "white"
  }
})

export default connect(()=>({}), { signUp })(AuthRegisterScreen)