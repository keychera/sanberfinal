export default (state = {
  userToken: null,
  username: null
}, action) => {
  switch(action.type) {
    case "SIGNIN":
      return {...state,
         userToken: action.payload.hash,
         username: action.payload.username
        }
    case "SIGNUP":
      return {...state,
        userToken: action.payload.hash,
        username: action.payload.username
       }
    case "SIGNOUT":
      return {...state,
        userToken: null,
        username: null
      }
    default:
      return state
  }
}